package com.devcamp.j62.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j62.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer> {
    CDistrict findById(int district_id);
}
