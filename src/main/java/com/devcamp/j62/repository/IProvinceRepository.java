package com.devcamp.j62.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j62.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Integer> {
    CProvince findById(int province_id);
}
