package com.devcamp.j62.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j62.model.CWard;

public interface IWardRepository extends JpaRepository<CWard, Long> {
    
}
