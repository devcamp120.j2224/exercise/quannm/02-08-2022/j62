package com.devcamp.j62.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j62.model.CDistrict;
import com.devcamp.j62.model.CWard;
import com.devcamp.j62.repository.IDistrictRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CWardController {
    
    @Autowired
    IDistrictRepository pDistrictRepository;

    @GetMapping("/wards")
    public ResponseEntity<Set<CWard>> getWardOfDistrict(@RequestParam int districtId) {
        try {
            CDistrict vDistrict = pDistrictRepository.findById(districtId);

            if (vDistrict != null) {
                return new ResponseEntity<Set<CWard>>(vDistrict.getWards(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
