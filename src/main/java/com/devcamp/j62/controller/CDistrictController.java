package com.devcamp.j62.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j62.model.CDistrict;
import com.devcamp.j62.model.CProvince;
import com.devcamp.j62.repository.IDistrictRepository;
import com.devcamp.j62.repository.IProvinceRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDistrictController {

    @Autowired
    IProvinceRepository pIProvinceRepository;

    @GetMapping("/districts")
    public ResponseEntity<Set<CDistrict>> getDistrictOfProvince(@RequestParam int provinceId) {
        try {
            CProvince vProvince = pIProvinceRepository.findById(provinceId);

            if (vProvince != null) {
                return new ResponseEntity<Set<CDistrict>>(vProvince.getDistricts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
