package com.devcamp.j62.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ward")
public class CWard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ward_id;

	private String name;

	private String prefix;

    @ManyToOne
    @JoinColumn(name = "district_id")
	private CDistrict district;
    
    public CWard() {
    }

    public CWard(String name, String prefix) {
        this.name = name;
        this.prefix = prefix;
    }

    public int getId() {
        return ward_id;
    }

    public void setId(int ward_id) {
        this.ward_id = ward_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
