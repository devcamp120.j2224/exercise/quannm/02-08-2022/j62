package com.devcamp.j62;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J62Application {

	public static void main(String[] args) {
		SpringApplication.run(J62Application.class, args);
	}

}
